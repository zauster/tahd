library(data.table)
library(testthat)
library(stringr)
library(here)
setwd(here::here())

source("R/00_Configuration.R")
load("data/TAHD.RData")


##
## melt to long format, creating an "treaty.ind" indicator
id.cols <- c("ID", "country1.iso3", "country2.iso3", "taname")
tahd.long <- melt(tahd, id.vars = c(id.cols, conf$year.columns),
                  variable.name = "treaty.ind", variable.factor = FALSE)


## I think with TAHD it makes only sense to use "entry"
conf$year.columns <- "entry"
for(year.col in conf$year.columns) {
    message("Working on '", year.col, "' as indicator of new treaty")

    tahd.tmp <- copy(tahd.long)

    ## delete the other column
    other.col <- setdiff(c("entry", "year"), year.col)
    tahd.tmp[, (other.col) := NULL]
    tahd.tmp <- unique(tahd.tmp)

    id.cols <- c("country1.iso3", "country2.iso3")
    tahd.tmp[, ID := NULL]
    tahd.tmp[, taname := NULL]
    tahd.tmp <- tahd.tmp[, .(value = max(value)), by = c(id.cols, "entry", "treaty.ind")]

    ## dcast to wide, using the signing year/entry year
    dcast.form <- paste0(paste0(c(id.cols, "treaty.ind"), collapse = " + "),
                         " ~ ", year.col)
    tahd.wide <- dcast(tahd.tmp, formula = dcast.form, value.var = "value")

    ## include year-columns that might be missing
    neededCols <- as.character(conf$firstYear:conf$lastYear)
    includedCols <- names(tahd.wide)[str_detect(names(tahd.wide), "[0-9]{4}")]
    missingCols <- setdiff(neededCols, includedCols)
    if(length(missingCols) > 0) {
        tahd.wide[, (missingCols) := as.numeric(NA)]
    }

    ## cast everything from the first year to the next year
    for(this.year in as.character((conf$firstYear + 1L):conf$lastYear)) {
        last.year <- as.character(as.integer(this.year) - 1)
        message(" / ", this.year, appendLF = FALSE)
        tahd.wide[is.na(get(this.year)) |
                  (!is.na(get(this.year)) & (get(this.year) < get(last.year))),
        (this.year) := get(last.year)]
    }
    cat("\n")

    ## TODO search for reporter-partner-year with more than one treaty

    ## TODO what about treaties where a party resigns from it, i.e. Brexit?

    ## melt again to long format
    year.col.string <- paste0(year.col, ".string")
    tahd.tmp <- melt(tahd.wide, id.vars = c(id.cols, "treaty.ind"),
                     variable.name = year.col.string, variable.factor = FALSE)
    tahd.tmp[, (year.col) := as.integer(get(year.col.string))]
    tahd.tmp[, (year.col.string) := NULL]

    ## where we didn't fill any value from a preceding year, we
    ## will insert "0" now
    tahd.tmp[is.na(value), value := 0]


    ## dcast to wide, using the treaty indicator variable
    dcast.form <- paste0(paste0(c(id.cols, year.col), collapse = " + "),
                         " ~ ", "treaty.ind")
    tahd.panel <- dcast(tahd.tmp, formula = dcast.form, value.var = "value")

    ## save the panel
    save(tahd.panel, file = paste0("data/TAHD_panel_by", str_to_title(year.col), ".RData"))
}
